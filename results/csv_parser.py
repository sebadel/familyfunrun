#!/usr/bin/env python

"""
It reads a CSV file
The CSV file is 3 fields separated by spaces
It produces an output which is meant to be copy-pasted in Results.vue as an array.
Example valid output:
            [1, '17:01:35', '558', 'Tim Schepers'],
            [2, '17:01:43', '563', 'Siegfried Ysebaert'],
            [3, '17:01:47', '565', 'Xander Ysebaert'],
"""

import csv
import json
import os
import re


def ReadCSV(filename):
  with open(filename, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    contents = [row for row in spamreader]
  return contents


def CreateTab(contents):
  # [1, '1:26:00', '555', 'name']
  output = []
  for i, line in enumerate(contents):
    while ('' in line):
      line.remove('')
    output.append("%s[%d, '%s', '%s', '%s']," % (' '*12, i+1, line[0], line[1], ' '.join(line[2:])))
  return '\n'.join(output)


def Batch():
  files = [
    '4km_dames.csv',
    '4km_hommes.csv',
    '4km_juniors.csv',
    '4km_seniors.csv',
    '7km_dames.csv',
    '7km_hommes.csv',
    '7km_juniors.csv',
    '7km_seniors.csv',
    '11km_dames.csv',
    '11km_hommes.csv',
    '11km_juniors.csv',
    '11km_seniors.csv',
  ]
  output = []
  for filename in files:
    print 'dede'
    race = filename.split('.')[0]
    output.append( """%s'%s': [
%s
%s],""" % (' '*10, race, CreateTab(ReadCSV(filename)), ' '*10))
  print '\n'.join(output)

#contents = ReadCSV('11km_seniors.csv')
#print CreateTab(contents)
Batch()
