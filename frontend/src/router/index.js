import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Information from '@/components/Information'
import Program from '@/components/Program'
import RegisterForm from '@/components/RegisterForm'
import Contact from '@/components/Contact'
import Photos from '@/components/Photos'
import Results from '@/components/Results'
import Thanks from '@/components/Thanks'
import { StripeCheckout } from 'vue-stripe'

Vue.use(Router)
Vue.component('stripe-checkout', StripeCheckout)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/inscription',
      name: 'RegisterForm',
      component: RegisterForm
    },
    {
      path: '/programme',
      name: 'Program',
      component: Program
    },
    {
      path: '/information',
      name: 'Information',
      component: Information
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/photos',
      name: 'Photos',
      component: Photos
    },
    {
      path: '/resultats',
      name: 'Results',
      component: Results
    },
    {
      path: '/thanks',
      name: 'Thanks',
      component: Thanks
    }
  ]
})
