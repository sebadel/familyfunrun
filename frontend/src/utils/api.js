import Vue from 'vue'

const _ENDPOINT = process.env.API_ENDPOINT
export function saveRunners (jsonPayload) {
  var v = new Vue()
  var url = _ENDPOINT + '/register'
  //      url += '?where={"id_client":' + idClient + '}'
  var run = v.$http.post(
    encodeURI(url),
    jsonPayload
  ).then(
    function (response) {
      return response
    }
  )
  return run
}

export function chargeCreditCard (jsonPayload) {
  var v = new Vue()
  var url = _ENDPOINT + '/charge'
  //      url += '?where={"id_client":' + idClient + '}'
  var run = v.$http.post(
    encodeURI(url),
    jsonPayload
  ).then(
    function (response) {
      return response
    }
  )
  return run

}