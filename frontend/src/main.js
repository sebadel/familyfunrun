// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill'
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import App from './App'
import Resource from 'vue-resource'
import router from './router'
import VueAnalytics from 'vue-analytics'

Vue.config.productionTip = false
Vue.use(Resource)
Vue.use(Vuex)
const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

const isProd = process.env.NODE_ENV === 'production'

Vue.use(VueAnalytics, {
  id: 'UA-122060813-1', // Under seb@delneste.be
  router,
  debug: {
    enabled: !isProd,
    sendHitTask: isProd
  }
})

const store = new Vuex.Store({
  state: {
    registration: {},
    runners: [],
    relais: {
      team: '',
      runners: []
    },
    garderie: {
      'lt3': 0,
      'gt3': 0
    },
    stripe: {},
  },
  getters: {
    registration (state) {
      return state.registration
    },
    runners (state) {
      return state.runners
    },
    relais (state) {
      return state.relais
    },
    garderie (state) {
      return state.garderie
    },
    stripe (state) {
      return state.stripe
    },
  },
  mutations: {
    storeSet (state, payload) {
      state[payload.name] = payload.value
    },
    clearRunners (state) {
      while (state.runners.length > 0) {
        state.runners.pop()
      }
    }
  },
  plugins: [vuexLocal.plugin]
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store: store,
  router: router,
  components: { App },
  template: '<App/>'
})
