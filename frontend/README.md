# familyfunrun

> Family Fun Run

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

Deploy frontend on server:
npm run build
ssh bigup-ce1 mkdir /webspace/familyfunrun/tmp
scp -r dist/* bigup-ce1:/webspace/familyfunrun/tmp/
ssh bigup-ce1 rm -rf /webspace/familyfunrun/public/*
ssh bigup-ce1 cp -a /webspace/familyfunrun/tmp/* /webspace/familyfunrun/public/
ssh bigup-ce1 rm -rf /webspace/familyfunrun/tmp


The backend is supposed to be exposed at https://backend.familyfunrun.be
The frontend is supposed to be exposed at https://www.familyfunrun.be with alias of https://familyfunrun.be

Issues on IE9, IE10 and IE11 => background only
Testing tools
 - netrenderer.com (might not render sub pages and keep showing homepage.)
 - browserstack.com
 - browserling.com (working well but limit per day.)
    https://www.browserling.com/browse/win/vista/ie/10/http%3A%2F%2Ffamilyfunrun.be

Issue 1:
  background opacity settings expressed in HEX (ex: #fffd) are not supported in IE or Edge, use rgba(255,255,255,0.7) equivalent :(

Issue 2:
  fixed with .babelrc file


