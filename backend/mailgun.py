import requests
import json
import os


if 'ENVIRONMENT' in os.environ:
  ENV = os.environ['ENVIRONMENT']
else:
  ENV = 'test'

MAILGUN_KEY_FILE = os.path.join(
    os.path.dirname(__file__),
    'config/mailgun/mailgun.json')

config = json.loads(open(MAILGUN_KEY_FILE).read())
sandbox = config[ENV]['sandbox']
key = config[ENV]['key']
sender = config[ENV]['sender']
bcc = config[ENV]['bcc']


def composeEmail(json_payload):
  token = json_payload['registration']['token']
  email = json_payload['registration']['email']
  total = json_payload['registration']['payment_total']
  payment_method = json_payload['registration']['payment_method']
  subject = 'FamilyFunRun - Confirmation d\'inscription - %s' % token
  if payment_method == "bank_transfer":
    payment_info = '\n'.join([
      'Afin de confirmer votre inscription, merci de verser %s EUR',
      'sur le compte de l\'association des parents BE76 9731 1959 7495',
      'avec pour communication "%s - %s"'
    ]) % (total, token, email)
  else:
    payment_info = 'Vous avez choisi le paiement par carte de credit.'
  body = '\n'.join([
    'Merci pour votre inscription',
    'Votre token est le %s' % token,
    '',
    payment_info,
    '',
    'Au plaisir de vous retrouver en pleine forme le 7 octobre 2018',
    'Toutes les infos sur le site https://familyfunrun.be'
  ])
  return subject, body

def sendEmail(recipient, subject, body):
  request_url = 'https://api.mailgun.net/v2/{0}/messages'.format(sandbox)
  request = requests.post(request_url, auth=('api', key), data={
      'from': sender,
      'to': recipient,
      'bcc': ','.join(bcc),
      'subject': subject,
      'text': body
  })
  print 'Status: {0}'.format(request.status_code)
  print 'Body:   {0}'.format(request.text)
  return request.status_code, request.text
