
from flask import Flask
from flask import request
import json
import os
import gsheets
import mailgun
import stripe_lib
import logging

if 'ENVIRONMENT' in os.environ:
  ENV = os.environ['ENVIRONMENT']
else:
  ENV = 'test'

app = Flask(__name__)


def _sendEmail(payload):
  recipient = payload['registration']['email']
  subject, body = mailgun.composeEmail(payload)
  mailgun.sendEmail(recipient, subject, body)

def _Charge(payload):
  token = payload['stripe']['token']
  description = 'Inscription Family Fun Run - %s' % token
  amount = payload['stripe']['amount']
  stripe_token = payload['stripe']['stripe_token']
  result = stripe_lib.Charge(amount, description, stripe_token)
  print result
  return result

@app.route('/register', methods=['GET', 'POST'])
def signup():
  gservice = gsheets.Auth()
  payload = request.get_json()
  print payload
  logging.info(payload)
  result = gsheets.PushJsonToGSheet(gservice, payload, ['registration', 'runners', 'relais', 'garderie'], ENV)
  _sendEmail(payload)
  return "result: %s" % result

@app.route('/charge', methods=['GET', 'POST'])
def charge():
  gservice = gsheets.Auth()
  payload = request.get_json()
  print payload
  logging.info(payload)
  stripe_result = _Charge(payload)
  payload['stripe']['status'] = stripe_result['status']
  payload['stripe']['amount'] = stripe_result['amount']
  result = gsheets.PushJsonToGSheet(gservice, payload, ['stripe'], ENV)
#  _sendEmail(payload)
  return "result: %s" % result

@app.after_request
def after_request(response):
  if 'Origin' in request.headers:
    response.headers.set(
      'Access-Control-Allow-Origin', request.headers['Origin'])
  response.headers.set(
    'Access-Control-Allow-Headers',
    'Content-Type, Authorization, Cache-Control, Pragma, Expires')
  response.headers.set(
    'Access-Control-Allow-Methods',
    'GET, PUT, POST, PATCH, DELETE, OPTIONS')
  return response
