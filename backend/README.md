Create a dev environment:
cd backend
virtualenv venv
. venv/bin/activate

Run a dev version:
$ export FLASK_ENV=development
$ export FLASK_APP=server.py
$ python -m flask run
 * Running on http://127.0.0.1:5000/


Install on Debian


# SSL
wget https://dl.eff.org/certbot-auto
chmod a+x certbot-auto
./certbot-auto
/webspace/familyfunrun/certbot/certbot-auto renew
# Add it to crontab

# Add wsgi support
sudo apt-get install libapache2-mod-wsgi

# Install venv
cd backend
virtualenv venv
. venv/bin/activate
pip install Flask
# If pip version is too old (Debian Wheezy), run this
pip install --upgrade --index-url=https://pypi.python.org/simple/ pip==8.1.2

pip install requests
pip install oauth2client
pip install apiclient
pip install --upgrade google-api-python-client
pip install stripe


