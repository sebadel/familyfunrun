import json
import os
import stripe

if 'ENVIRONMENT' in os.environ:
  ENV = os.environ['ENVIRONMENT']
else:
  ENV = 'test'

STRIPE_KEY_FILE = os.path.join(
    os.path.dirname(__file__),
    'config/stripe/stripe.json')

config = json.loads(open(STRIPE_KEY_FILE).read())
stripe.api_key = config[ENV]['secretStripeKey']
def Charge(amount_eur, description, token, currency='eur'):
  stripe_result = stripe.Charge.create(
      amount=amount_eur * 100,
      currency='eur',
      description=description,
      source=token
  )
  return stripe_result
