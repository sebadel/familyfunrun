from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http
from apiclient import discovery
import os
#result = service.spreadsheets().values()

# Don't forget to give grant edit rights on your spreadsheet to your service account.
if 'ENVIRONMENT' in os.environ:
  ENV = os.environ['ENVIRONMENT']
else:
  ENV = 'test'

SPREADSHEETS = {
    'test': '1E1wbMiehMfqZAKG480RLuu6vapet0-hQse-FjtPJhZQ',
    'prod': '18ibVlT5EqnfRStQsQJFcsv5TrdZHMbmapJx9eIZZz8E'
}
SHEETS = {
    'registration': 'Info',
    'runners': 'Runners',
    'relais': 'Relais',
    'garderie': 'Garderie',
    'stripe': 'Stripe'
}
SHEET_HEADERS = {
  'registration': ['token', 'email', 'phone', 'payment_total', 'payment_method', 'payment_status', 'gift', 'datetime'],
  'runners': ['token', 'email', 'lastname', 'firstname', 'birthday', 'sex', 'price', 'race'],
  'relais': ['token', 'email', 'team', 'lastname', 'firstname', 'birthday'],
  'garderie': ['token', 'email', 'lt3', 'gt3'],
  'stripe': ['token', 'email', 'stripe_token', 'amount', 'status'],
}

SERVICE_ACCOUNT_JSON_FILE = os.path.join(
    os.path.dirname(__file__),
    'config/gdrive/ffr.json')

def Auth():
  scopes = ['https://www.googleapis.com/auth/spreadsheets']
  # This JSON file is generated as a service account in the Google Dev console.
  credentials = ServiceAccountCredentials.from_json_keyfile_name(
      SERVICE_ACCOUNT_JSON_FILE, scopes)
  http_auth = credentials.authorize(Http())
  discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?version=v4')
  service = discovery.build('sheets', 'v4',
                            http=http_auth, discoveryServiceUrl=discoveryUrl)
  return service


def _InfoToRows(json_payload):
  headers = SHEET_HEADERS['registration']
  sheet_name = SHEETS['registration']
  _range = '%s!A2:H2' % sheet_name
  rows = []
  row = []
  for k in headers:
    row.append(json_payload['registration'][k])
  rows.append(row)
  return _range, rows

def _RunnersToRows(json_payload):
  headers = SHEET_HEADERS['runners']
  sheet_name = SHEETS['runners']
  _range = '%s!A2:H2' % sheet_name
  rows = []
  for runner in json_payload['runners']:
    row = [json_payload['registration']['token'], json_payload['registration']['email']]
    for k in headers[2:]:
      row.append(runner[k])
    rows.append(row)
  return _range, rows

def _RelaisToRows(json_payload):
  headers = SHEET_HEADERS['relais']
  sheet_name = SHEETS['relais']
  _range = '%s!A2:F2' % sheet_name
  rows = []
  for runner in json_payload['relais']['runners']:
    if not json_payload['relais']['team'] and not runner['lastname'] and not runner['firstname']:
      continue
    row = [json_payload['registration']['token'],
           json_payload['registration']['email'],
           json_payload['relais']['team'],
    ]
    for k in headers[3:]:
      row.append(runner[k])
    rows.append(row)
  return _range, rows

def _GarderieToRows(json_payload):
  headers = SHEET_HEADERS['garderie']
  sheet_name = SHEETS['garderie']
  _range = '%s!A2:D2' % sheet_name
  row = [json_payload['registration']['token'], json_payload['registration']['email']]
  for k in headers[2:]:
    row.append(json_payload['garderie'][k])
  return _range, [row]

def _StripeToRows(json_payload):
  headers = SHEET_HEADERS['stripe']
  sheet_name = SHEETS['stripe']
  _range = '%s!A2:D2' % sheet_name
  row = [json_payload['stripe']['token'], json_payload['stripe']['email']]
  for k in headers[2:]:
    row.append(json_payload['stripe'][k])
  return _range, [row]

def DataToRows(sheet, json_payload):
  if sheet == 'registration':
    _range, rows = _InfoToRows(json_payload)
  if sheet == 'runners':
    _range, rows = _RunnersToRows(json_payload)
  if sheet == 'relais':
    _range, rows = _RelaisToRows(json_payload)
  if sheet == 'garderie':
    _range, rows = _GarderieToRows(json_payload)
  if sheet == 'stripe':
    _range, rows = _StripeToRows(json_payload)
  return _range, rows


def PushJsonToGSheet(service, json_payload, sheets, env=ENV):
  """Args:
    json_payload: {
      registration: {},
      garderie: {},
      runners: []
      relais_runners: [],
    }
  """
  # info

  value_input_option = 'USER_ENTERED'
  insert_data_option = 'INSERT_ROWS'
  spreadsheet = SPREADSHEETS[env]
  for sheet in sheets:
    _range, rows = DataToRows(sheet, json_payload)
    request = service.spreadsheets().values().append(
        spreadsheetId=spreadsheet,
        range=_range,
        valueInputOption=value_input_option,
        insertDataOption=insert_data_option,
        body={'values': rows})
    response = request.execute()
#  print response
#  return response


def ReadGSheet(service, env='test'):
  RANGE_NAME = 'Sheet1!A1:E10'
  result = service.spreadsheets().values().get(spreadsheetId=SPREADSHEET_ID,
                                               range=RANGE_NAME).execute()
  values = result.get('values', [])
  if not values:
    print('No data found.')
  else:
    print('Name, Major:')
    for row in values:
        # Print columns A and E, which correspond to indices 0 and 4.
        print('%s, %s' % (row[0], row[4]))
  return values

