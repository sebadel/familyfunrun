#!/webspace/familyforrun/backend/venv/bin/python

import os
import sys
sys.path.insert(0, '/webspace/familyfunrun/backend/venv/bin')
sys.path.insert(0, '/webspace/familyfunrun/backend/venv')
sys.path.insert(0, '/webspace/familyfunrun/backend/')
sys.path.insert(0, '/webspace/familyfunrun/backend/venv/lib/python2.7/site-packages')
os.environ['ENVIRONMENT'] = 'prod'

from server import app as application
